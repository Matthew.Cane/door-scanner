from picamera import PiCamera
from pyzbar.pyzbar import decode
from PIL import Image
from io import BytesIO
from gpiozero import Buzzer, LED
from threading import Thread
from string import ascii_letters
import random
#from distutils.util import strtobool
import time
import sys
import datetime
import requests
import json

def buzz(mode):
	if quiet or mode == 0:
		return
	if mode == 1:
		buzzer.on()
		time.sleep(1)
		buzzer.off()
		return
	elif mode == 2:
		for i in range(0,2):
			buzzer.on()
			time.sleep(.1)
			buzzer.off()
			time.sleep(.1)
		return
	elif mode == 3:
		for i in range(0,5):
			buzzer.on()
			time.sleep(.1)
			buzzer.off()
			time.sleep(.1)
		return

def led(mode):
	if mode == 0:
		for i in range (0,25):
			ledG.on()
			ledR.on()
			ledY.on()
			time.sleep(0.1)
			ledR.off()
			ledY.off()
			ledG.off()
			time.sleep(0.1)

	elif mode == 1:
		ledG.on()
		time.sleep(1)
		ledG.off()
		return
	elif mode == 2:
		ledR.on()
		time.sleep(1)
		ledR.off()
		return
	elif mode == 3:
		for i in range(0,5):
			ledY.off()
			time.sleep(.1)
			ledY.on()
			time.sleep(.1)
		return

def outputAlert(mode):
	# Mode 0 to flash all LEDs, no buzz
	# Mode 1 is for successful unlock
	# Mode 2 is for unsuccessful unlock
	# Mode 3 is for general system error

	led_thread = Thread(target=led, args=(mode,))
	buzz_thread = Thread(target=buzz, args=(mode,))

	led_thread.start()
	buzz_thread.start()

def getDateTime():
	return datetime.datetime.now().replace(microsecond=0).isoformat()

def logEvent(eventType, message):
	logMessage = "{}\t[{}]\t{}\n".format(getDateTime(), eventType.upper(), message)
	print(logMessage[:-1]) #[:-1] strips endline for printing
	logFile.write(logMessage)
	# Add API log insert call here

def apiLogEvent(userID, authorised):
        data = {"user_id":userID, "door_id":doorID, "zone_authorised":True, "key_authorised":authorised}
        requests.post(API_URL + "/api/logs", data=data, timeout=1)

def validate(token):
	if token.type != "QRCODE":
		logEvent("scan", "Token is invalid code type: " + token.type)
		outputAlert(2)
		return False
        if len(token.data) != 128:
		logEvent("scan", "Token is of invalid length: " + token.data)
		outputAlert(2)
		return False
	if auth(*parseToken(token.data)):
                apiLogEvent(parseToken(token.data)[0], True)
                logEvent("scan", "Token is authorised: " + token.data)
		outputAlert(1)
		unlockDoor()
		return True
	else:
		logEvent("scan", "Token not authorised: " + token.data)
                apiLogEvent(parseToken(token.data)[0], False)
		outputAlert(2)
		return False

def auth(userID, userKey):
	if debug: # Debug with known working codes
		print("using debug auth")
		import testCodes
		print("imported testcodes")
		print("unparsed :" + userID + userKey)
		if userID+userKey == testCodes.true:
			return True
		elif  userID+userKey == testCodes.false:
			return False

	message = {"UID": userID, "UKEY": userKey, "DID": doorID}
	response = requests.post(API_URL + "/api/auth", data=message, timeout=1)
	if debug:
		print("Auth Response: "+str(response))
	response = json.loads(response.text)
        return response["authorised"]
        

def parseToken(token):
	userID = token[:64]
	userKey = token[64:]
	return userID, userKey

def unlockDoor():
	# Door unlocking code would be here
	# in a production system with electronic
	# locks connected to the GPIO pins
	time.sleep(2)
	return

def testConnection():
	try:
		response = requests.get(API_URL + "/api/heartbeat", timeout=1)
		if response.status_code == 200:
			return True
	except:
		return False
	return False

def generateID(length):
	ID = ""
	characters = ascii_letters
	for i in range (0,10):
		characters += str(i)
	for i in range(0,length):
		ID += random.choice(characters)
	return ID

def getDoorID():
	try:
		IDfile = open("id.env")
		return IDfile.read()
	except:
		IDfile = open("id.env", "w")
		newID = generateID(64)
		IDfile.write(newID)
		return newID

def getAPIAddress():
	try:
		apiFile = open("api.env")
		return apiFile.readline().strip()
	except:
		return None

def captureStream():
	stream.seek(0)
	camera.capture(stream, use_video_port=VMode, format="jpeg")
	frame = Image.open(stream)
	detected = decode(frame) # Returns list of ebjects of detected codes
	if len(detected) == 0:
		return False
	validate(detected[0])
	return
#-------------------------------
# START OF SYSTEM INIT
#-------------------------------
					#		oo
ledG = LED(2)		#		oo <-
ledY = LED(3)		#		oo <-
ledR = LED(4)		#		oo <-
# GRND				#		oo <-
buzzer = Buzzer(17)	#		oo <-

ledY.on() #Power LED
debug = False
quiet = False
VMode = True
sensorMode = [(1920, 1080), (2592, 1944), (2592, 1944), (1296, 972), (1296, 730), (640, 480), (640, 480)]
resolution = sensorMode[3]

#-------------------------------
# ARG PROCESSING
#-------------------------------

args = sys.argv
if "--debug" in args:
	debug = True
if "--quiet" in args:
	quiet = True
if "--smode" in args:
	index = args.index("--smode")+1
	smode = int(args[index])-1
	resolution = sensorMode[smode]

#-------------------------------
# CAMERA INIT
#-------------------------------

camera = PiCamera()
camera.resolution = resolution
camera.framerate = 30
camera.contrast = 100
camera.color_effects = (128, 128) #B&W only

stream = BytesIO()
if debug:
	camera.start_preview()
	time.sleep(2)

#-------------------------------
# LOG INIT
#-------------------------------

logFile = open("/var/log/scanner.log", "a+")

logEvent("init", "System initialised with settings: debug={}, quiet={}, vmode={}, resolution={}".format(debug,quiet,VMode,resolution))

doorID = getDoorID()
API_URL = getAPIAddress()

if debug:
	logEvent("debug", "Door ID is: " + doorID)
	logEvent("debug", "API URL is: " + API_URL)


#-------------------------------
# API CONNECTION INIT
#-------------------------------

while not testConnection():
	logEvent("conn", "Unable to establish connection to API. Waiting 5 seconds and retrying")
	outputAlert(3)
	time.sleep(5)
#-------------------------------
# END OF SYSTEM INIT
#-------------------------------
if debug:
	logEvent("conn", "Successfully establish connection to API")

while True: #Main loop
	captureStream()
